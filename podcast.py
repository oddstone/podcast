import sqlite3
import urllib.request
import os.path
import shutil
import re

class database:
    def __init__(self, name):
        self.conn = sqlite3.connect(name)
        c = self.conn.cursor()
        exist = c.execute('select * from sqlite_master where name = ?',('urls',)).fetchone()
        if exist == None:
            c.execute('create table urls (url varchar primary key)')
            self.conn.commit()
    
    def query(self, url):
        c = self.conn.cursor()
        q = "SELECT * FROM urls WHERE url = '%s'"%url
        c.execute(q)
        return c.fetchone()

    def is_duplicate(self, url):
        return self.query(url) != None

    def insert(self, url):
        c = self.conn.cursor()
        c.execute("INSERT INTO urls VALUES (?)", (url,))
        self.conn.commit()

def url_open(url, h = {}):
    req = urllib.request.Request(url, headers=h)
    req.set_proxy('127.0.0.1:8087','http')            #change your proxy here
    r = urllib.request.urlopen(req)
    return r

def get_urls(rss):
    r = url_open(rss)
    urls = re.findall(r'<enclosure url="(.*?)"', r.read().decode("utf8"))
    return urls

def get_file_name_with_slash(url):
    s = url.rfind('/')
    e = url.find('?', s)
    if (e == -1):
        return url[s:]
    return url[s:e]

def open_file(loc):
    if (os.path.exist(loc)):
        return open(loc, "ab")
    return open(loc, "wb")

def download(dir, url):

    dst = dir + get_file_name_with_slash(url);
    src = dst +'.tmp'
    headers = {}
    if (os.path.exists(src)):
        have = os.path.getsize(src)
    else:
        have = 0
    if (have > 0):
        print("resume from %d"%have)
        f = open(src, "ab")
        headers["Range"] = ("bytes=%s-" % (have))
    else:
        f = open(src, "wb")
    print("downloading %s"%url)

    r = url_open(url, headers)
    total = r.length
    while have < total:
        b = r.read(512*1024)
        if not b:
            break
        f.write(b)
        have += len(b)
        print("%d / %d   %f"%(have,total,(have/total)))
    f.close()
    if (have == total):
        shutil.move(src, dst)
        return True
    return False


def get_current_dir():
    return os.path.dirname(os.path.abspath(__file__));



podcasts = [

           ['mtp',      'http://podcastfeeds.nbcnews.com/audio/podcast/MSNBC-MTP-NETCAST-M4V.xml'],
           ['today',    'http://podcastfeeds.nbcnews.com/audio/podcast/MSNBC-TDY-PODCAST-M4V.xml'],
           ['maddow',   'http://podcastfeeds.nbcnews.com/audio/podcast/MSNBC-MADDOW-NETCAST-M4V.xml'],
           ['nightly',  'http://podcastfeeds.nbcnews.com/audio/podcast/MSNBC-NN-NETCAST-M4V.xml'],
           #['BeautifulPlaces', 'http://www.ustudio.com/feeds/SfikF0sYtjBU/itunes.xml?profile=appletv'],
           ['boyt',     'http://feeds.feedburner.com/boyt'],
           ['WorldNews', 'http://www.nytsyn.com/rss/custom_itunes/4'],
           ['DemocracyNow', 'http://www.democracynow.org/podcast-video.xml'],
           ['TedBussiness', 'http://feeds.feedburner.com/iTunesPodcastTTBusiness'],
           ['nasa', 'http://www.nasa.gov/rss/dyn/NASAcast_vodcast.rss']
          ]

db = database("downloaded.db")
for pod in podcasts:
    urls = get_urls(pod[1])
    print(urls)
    dir = get_current_dir() + '/' + pod[0]
    if (os.path.exists(dir) == False):
        os.mkdir(dir)

    for url in urls:
        if (not db.is_duplicate(url) and download(dir, url)):
            db.insert(url)

